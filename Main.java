package correctPath;

/*
 * Input :"???rrurdr?"
 * Output:"dddrrurdrd"
 * Input:"drdr??rrddd?"
 * Output:"drdruurrdddd"
 */
public class Main extends Path {

	public static void main(String[] args)
	{
		String path1 = "???rrurdr?" ; 
		String expected1 = "dddrrurdrd" ;
		
		String path2 = "drdr??rrddd?" ;
		String expected2 = "drdruurrdddd" ;
		
		/*if (checkPath(expected1) && checkPath(expected2))
		{
			System.out.println("�a marche.") ;
		}*/
		
		System.out.println("Path 1 : " + path1) ;
		System.out.println("Expected 1 : " + expected1 + "\n") ;
		
		solvePath(path1) ;
		System.out.println(" ");
		
		System.out.println("Path 2 : " + path2) ;
		System.out.println("Expected 2 : " + expected2 + "\n") ;
		
		solvePath(path2) ;
		System.out.println(" ");
	}

}
