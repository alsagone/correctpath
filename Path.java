package correctPath;

public class Path extends Position
{
	//Check if the path given in argument leads exactly to the exit.
	public static boolean checkPath(String path)
	{
		Position p = new Position(1,1) ;
		Position exit = new Position(5,5) ;
		int i ; 
		char direction ;

		for (i = 0 ; i < path.length() ; i++)
		{
			direction = path.charAt(i) ;
			
			if (!p.canMove(direction))
			{
				return false ;
			}
			
			p.move(direction) ;
		}

		return p.equalPosition(exit) ; 
	}

	public static String setDirection (char direction, String path, int index)
	{
		StringBuilder s = new StringBuilder(path) ; 
		
		if (path.charAt(index) == '?')
		{
			s.setCharAt(index,direction) ;
		}
		
		return s.toString() ; 
	}
	
	public static boolean solvePath (String path)
	{
		String listOfDirections = "udlr" ;
		String newPath ;
		int indexToFill, i ;

		char empty = '?' ;
		char direction ;
		
		boolean b ;
		
		//Path is full, we need to check if it leads to the exit.
		if (!path.contains("?"))
		{
			b = checkPath(path) ;
			
			if (b)
			{
				System.out.println("Solved path : " + path);
			}

			return b ;
		}
		
		else
		{
			for (i = 1 ; i <= 4 ; i++)
			{
				indexToFill = path.indexOf('?') ;
				direction = listOfDirections.charAt(i-1) ;
				newPath = setDirection(direction,path,indexToFill) ;
				b = solvePath(newPath) ;
				
				//If the path is incorrect, resets the direction to empty.
				if (!b)
				{
					newPath = setDirection(empty,newPath,indexToFill) ;
				}
				
				else 
				{
					return b ;
				}
			}
		}
		
		return false ;
	}
}
