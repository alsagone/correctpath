package correctPath;

public class Position
{
	int abscissa ; 
	int ordinate ; 

	public Position (int x, int y)
	{
		this.abscissa = x ;
		this.ordinate = y ;
	}
	
	public Position()
	{
		this.abscissa = 0 ;
		this.ordinate = 0 ; 
	}

	public String displayPosition()
	{
		return "(" + this.abscissa + " ; " + this.ordinate + ")" ;
	}

	public boolean canMove (char direction)
	{
		boolean b = false ;

		if ((direction == 'u') && (this.ordinate > 1))
		{
			b = true ;
		}

		else if ((direction == 'd') && (this.ordinate < 5))
		{
			b = true ;
		}

		else if ((direction == 'l') && (this.abscissa > 1))
		{
			b = true ;
		}

		else if ((direction == 'r') && (this.abscissa < 5))
		{
			b = true ; 
		}

		return b ;
	}

	public void move (char direction)
	{
		if (direction == 'u') 
		{
			this.ordinate-- ;
		}

		else if (direction == 'd')
		{
			this.ordinate++ ;
		}

		else if (direction == 'l')
		{
			this.abscissa-- ;
		}

		else if (direction == 'r')
		{
			this.abscissa++ ;
		}

		return ; 
	}

	public boolean equalPosition (Position p)
	{
		return ((this.abscissa == p.abscissa) && (this.ordinate == p.ordinate)) ;
	}
}
